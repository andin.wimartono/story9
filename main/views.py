from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse

#Create your views here
def index(request):
    return render(request, 'index.html')

def user_login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")        
        if not username or not password:
            messages.error(request, "Invalid Login, Please Try Again!")
            return render(request, 'login.html')
        user = authenticate(request,username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('main:loginsuccess')
        messages.error(request,"Wrong Username or Password")
        return render(request,'login.html')
    else:   
        return render(request, 'login.html')

def user_signup(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        if not username or not password:
            messages.error(request, "Please Fill out the Data Field")
            return render(request, 'signup.html')

        try:
            user = User.objects.get(username=username)
            messages.error(request, "Username is already taken by another user")
            return render(request, 'signup.html')
        except:
            user = User.objects.create_user(username=username, password=password)
            return redirect(reverse('main:index'))
    else:
        return render(request,'signup.html')

@login_required(login_url='main:index')

def user_loginsuccess(request):
    if request.method == "GET":
        return render(request, 'loginsuccess.html')

def user_logout(request):
    logout(request)
    return redirect('main:index')
