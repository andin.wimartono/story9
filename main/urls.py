from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.user_login, name='login'),
    path('loginsuccess', views.user_loginsuccess, name='loginsuccess'),
    path('logout', views.user_logout, name='logout'),
    path('signup', views.user_signup, name='signup')
]
